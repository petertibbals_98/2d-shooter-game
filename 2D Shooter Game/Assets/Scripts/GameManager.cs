﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {


	public static GameManager instance;
	

		// Astroid SpawnPoints
		public GameObject[] asteroidSpawnPoints;

		// Astroid prefab
		public float astroidSpeed;
		// List of Astroids
		public List<GameObject> astroids;

		// Bullet prefab
		public GameObject bullet;
		public float bulletSpeed;
		public float bulletLife;

		// Enemy ship
		public float enemyShipSpeed;
		public float enemyShipRotationSpeed;


	// List of enemies
	public List<GameObject> activeEnemies;
		public bool removingEnemies;
		public int maximumNumberActiveEnemies;

		public GameObject player;

		public int numLives;

	void Awake()
	{
		if (instance == null)
		{
			instance=this;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
	}

	// Use this for initialization
	void Start ()
	{

		activeEnemies = new List<GameObject>();
		removingEnemies = false;

	}
	
	// Update is called once per frame
	void Update () 
	{
				if (Input.GetKeyDown(KeyCode.R))
				{
					if (activeEnemies.Count < maximumNumberActiveEnemies)
					{
						// What spawn point
						int id = Random.Range(0, asteroidSpawnPoints.Length);
						GameObject point = asteroidSpawnPoints[id];

						//What asteroid?
						GameObject astroid = astroids[Random.Range(0, astroids.Count)];

						// Instantiate asteroid
						GameObject astroidInstance = Instantiate<GameObject>(astroid, point.transform.position, Quaternion.identity);

						Vector2 DirectionVect = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
						DirectionVect.Normalize();
						astroidInstance.GetComponent<Astroid>().direction = DirectionVect;


						// Add to enemies list
						activeEnemies.Add(astroidInstance);
					}
				}

	}
		//Gettting rid of the enemies
		public void RemoveEnemies()
		{
			removingEnemies = true;
			for (int i=0; i< activeEnemies.Count; i++)
			{
				Destroy(activeEnemies[i]);
			}
			activeEnemies.Clear();
			removingEnemies = false;
		}
		
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	Transform tf;
	public float speed;
	public float rotationSpeed;
	public int Score;
	public Text scoreText;
	public Text livesText;

	// Use this for initialization
	void Start ()
	{
		tf = GetComponent<Transform>();

		Score = 0;
		scoreText.text = "Score: " + Score;
		livesText.text = "Lives: " + GameManager.instance.numLives;
	}

	// Update is called once per frame
	void Update()
	{
		//Basic movement with arrows or WASD
		if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
		{
			tf.transform.Translate(Time.deltaTime * speed,0,0);
		}
		if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
		{
			tf.transform.Translate(-Time.deltaTime * speed, 0,0);
		}
		if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
		{
			tf.transform.Rotate(0, 0, Time.deltaTime * rotationSpeed);
		}
		if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
		{
			tf.transform.Rotate(0, 0, -Time.deltaTime * rotationSpeed);
		}

		if (Input.GetKey(KeyCode.H))
		{
			//Debug.Log("Space");
			//tf.transform.position = new Vector3(0, 0, 0);
		}

		if (Input.GetKeyDown(KeyCode.Space)) //Shooting, yay fun
		{
			GameObject bullet = Instantiate<GameObject>(GameManager.instance.bullet);
			bullet.transform.position = transform.position;
		}

	}

	void ScorePoints (int PointsToAdd)
	{
		Score += PointsToAdd;
		scoreText.text = "Score: " + Score;	
	}
		

	//Destoying stuff
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Astroid")
		{
			LifeLost();
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.tag == "BG")
		{
			LifeLost();
		}
	}


	void LifeLost()
	{
		GameManager.instance.numLives--;
		if (GameManager.instance.numLives == 0)
		{
			// Game over
			Destroy (this.gameObject);
			     //Debug.Log("Game Over!");
			livesText.text = "Lives: " + "Game Over!";
			StartCoroutine("WaitToClose");

			   //WaitForSecondsRealtime (5); //Nope need to do a cororutine for this apparently
			   //Application.Quit();
		}
		else
		{
			//GameManager.instance.RemoveEnemies();
			tf.transform.position = new Vector3(0, 0, 0);
			livesText.text = "Lives: " + GameManager.instance.numLives;
			//GameManager.instance.numLives -- ;

		}
	}

	IEnumerator WaitToClose () //Waiting so you can see your score before the game closes
	{
		yield return new WaitForSeconds (5);
		Debug.Log("Game Over!");
		Application.Quit();
	}

}

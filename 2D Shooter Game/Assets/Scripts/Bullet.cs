﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	private Vector3 direction;

	// Use this for initialization
	void Start () 
	{
		//Direction for where the bullet is going
		direction = GameManager.instance.player.transform.right;
		Destroy(this.gameObject, GameManager.instance.bulletLife);

	}

	// Update is called once per frame
	void Update () {

		transform.Translate(direction * Time.deltaTime * GameManager.instance.bulletSpeed);
	}
}

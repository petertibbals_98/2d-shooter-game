﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShip : MonoBehaviour {
	public Vector3 direction;
	public int points;
	public GameObject player;

	// Use this for initialization
	void Start () 
	{
		//direction = new Vector3(1, 0, 0);
		player = GameObject.FindWithTag ("Player");
	}

	// Update is called once per frame
	void Update () 
	{

		direction = GameManager.instance.player.transform.position - transform.position;
		direction.Normalize();
		float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

		// Targeting, makes it go after the player
		Quaternion targetRotation = Quaternion.Euler(0, 0, angle);
		transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, GameManager.instance.enemyShipRotationSpeed * Time.deltaTime);

		transform.Translate(Time.deltaTime * GameManager.instance.enemyShipSpeed, 0, 0);

	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Bullet") //Kills astroid if they touch a bullet
		{
			GameManager.instance.activeEnemies.Remove(this.gameObject);
			Destroy(this.gameObject);

			// Also destroy bullet
			Destroy(other.gameObject);

			//Giving player points
			player.SendMessage("ScorePoints",points);

		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (!GameManager.instance.removingEnemies) 
		{
			if (other.gameObject.tag == "BG")  //Kills ship if it leave the board
			{
				GameManager.instance.activeEnemies.Remove (this.gameObject);
				Destroy (this.gameObject);
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Astroid : MonoBehaviour {

	public Vector2 direction;
	public int points;
	public GameObject player;
	public Rigidbody2D rb;
	public float maxThrust;
	public float maxTorgue;

	// Use this for initialization
	void Start () 
	{
		player = GameObject.FindWithTag ("Player");
	}

	// Update is called once per frame
	void Update()
	{
		transform.Translate(direction * Time.deltaTime * GameManager.instance.astroidSpeed);

		Vector2 thrust = new Vector2 (Random.Range (-maxThrust, maxThrust), Random.Range (-maxThrust, maxThrust));
		float torgue = Random.Range (-maxTorgue, maxTorgue);

		rb.AddForce (thrust);
		rb.AddTorque (torgue);

        direction = GameManager.instance.player.transform.position - transform.position; //yep
        direction.Normalize(); //Yep
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg; //also test
        transform.rotation = Quaternion.Euler(0f, 0f, angle); //testing

    }


	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Bullet") //Kills astroid if they touch a bullet
		{
			GameManager.instance.activeEnemies.Remove(this.gameObject);
			Destroy(this.gameObject);

			// Also destroy bullet
			Destroy(other.gameObject);

			//Give player points/score
			player.SendMessage("ScorePoints",points);
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (!GameManager.instance.removingEnemies)
		{
			if (other.gameObject.tag == "BG") //Kills player if they leave the board
			{
				GameManager.instance.activeEnemies.Remove(this.gameObject);
				Destroy(this.gameObject);
			}
		}
	}

}
